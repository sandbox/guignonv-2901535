<?php

// require_once 'api/graphs_s_tripalphylo.api.php';

function graphs_s_tripalphylo_graphs_plugin_info() {
  return array(
    'graph_sourcer' => array(
      'tripal_phylo' => array(
        'module' => 'graphs_s_tripalphylo',
        'version' => '1.0.0',
        'compatibility' => array(
          'graph_sourcer' => array(
            'default' => FALSE,
          ),
          'graph_renderer' => array(
            'default' => TRUE,
          ),
          'graph_action' => array(
            'default' => TRUE,
          ),
        ),
      ),
    ),
  );
}

/**
 *
 */
function graphs_s_tripalphylo_graphs_form($graph_sourcer) {
  return array(
    '#type'=> 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
    '#title'       => t('Tripal Phylogeny options'),
    '#attributes' => array(
      'class' => array(
        'graph-datasource-type-tripal_phylo',
      ),
    ),

    'phylotree_id' => array(
      '#title' => t('Tree identifier'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_sourcer->phylotree_id) ? $graph_sourcer->phylotree_id : '',
      '#description' => t('Phylogeny tree identifier (phylotree_id) as stored in the phylotree Chado table.'),
      '#required' => TRUE,
      '#size' => 30,
    ),

    'include_node_types' => array(
      '#title' => t('Only include the specified node types'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_sourcer->include_node_types) ? $graph_sourcer->include_node_types : '',
      '#description' => t('Name of the types (coma separeted list) to take into account. It correspond to the type_id of the phylonode table.'),
      '#required' => FALSE,
      '#size' => 60,
    ),

    'exclude_node_types' => array(
      '#title' => t('Exclude the specified node types'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_sourcer->exclude_node_types) ? $graph_sourcer->exclude_node_types : '',
      '#description' => t('Name of the types (coma separeted list) to exclude. It correspond to the type_id of the phylonode table.'),
      '#required' => FALSE,
      '#size' => 60,
    ),

    'initial_depth' => array(
      '#title' => t('Initial number of sub-level to load'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_sourcer->initial_depth) ? $graph_sourcer->initial_depth : '1',
      '#description' => t('The number of sub-level to pre-load and that would not need to be loaded through an Ajax call. 0 means "load only root node".'),
      '#required' => FALSE,
      '#size' => 30,
    ),

    'default_depth' => array(
      '#title' => t('Default number of sub-level to load'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_sourcer->default_depth) ? $graph_sourcer->default_depth : '2',
      '#description' => t('The number of sub-level to load on Ajax calls if not specified. If not set, all the graph would be loaded.'),
      '#required' => FALSE,
      '#size' => 30,
    ),

  );
}

/**
 *
 */
function graphs_s_tripalphylo_graphs_validate($form, $form_state) {
  /*
   @todo:
   - check node types exist
   - check compatibility of inclusion/exclusion of node types
  */
}

/**
 *
 */
function graphs_s_tripalphylo_graphs_render($graph_sourcer) {
  $render_array = array();

  $nodes = array();
  $links = array();
  $graph = array(
    'nodes' => array(),
    'links' => array(),
  );

  // Get phylogeny root.
  $root = chado_generate_var(
    'phylonode',
    array(
      'phylotree_id' => $graph_sourcer->phylotree_id,
      'parent_phylonode_id' => NULL,
    )
  );

  if (!$root) {
    drupal_set_message('Tree root not found (phylotree_id ' . $graph_sourcer->phylotree_id . ' has no nodes with parent_phylonode_id not set)!', 'warning');
    return $render_array;
  }
  elseif (is_array($root) && (1 < count($root))) {
    drupal_set_message('Multiple roots found (phylotree_id ' . $graph_sourcer->phylotree_id . ' has multiple nodes without parent_phylonode_id)!', 'warning');
    return $render_array;
  }

  // Get node query parameters.
  $filter_relationships = array_filter(preg_split('/\s*,\s*/', $graph_sourcer->include_node_types));
  $exclude_relationships = array_filter(preg_split('/\s*,\s*/', $graph_sourcer->exclude_node_types));
  $parameters = array(
    'node_id' => $root->phylonode_id,
    'depth' => $graph_sourcer->initial_depth - 1,
    'rel_filter' => $filter_relationships,
    'rel_exclude' => $exclude_relationships,
    'skip_node_ids' => array($root->phylonode_id),
  );

  // Add root as a node.
  $graph['nodes'][] = $nodes[$root->phylonode_id] = array(
    'id' => $root->phylonode_id,
    'name' => $root->label,
    'type' => $root->type_id->name,
    'left_idx' => $root->left_idx,
    'right_idx' => $root->right_idx,
  );

  // Get root sub-graph.
  $sub_graph = graphs_s_tripalphylo_graphs_get_graph_data($parameters);

  // Merge sub-graph to global graph.
  foreach ($sub_graph['nodes'] as $sub_node) {
    if (!array_key_exists($sub_node['id'], $nodes)) {
      $graph['nodes'][] = $nodes[$sub_node['id']] = $sub_node;
    }
  }
  $graph['links'] = array_merge($graph['links'], $sub_graph['links']);
  graph_extract_links($graph, $links);

  $render_array['#settings'] = array(
    'type' => $graph_sourcer->type,
    'module' => 'graphs_s_tripalphylo',
    'init' => 'init',
    'node_index' => $nodes,
    'link_index' => $links,
    'graph' => $graph,
    // Parameters.
    'phylotree_id' => $graph_sourcer->phylotree_id,
    'include_node_types' => $graph_sourcer->include_node_types,
    'exclude_node_types' => $graph_sourcer->exclude_node_types,
    'initial_depth' => $graph_sourcer->initial_depth,
    'default_depth' => $graph_sourcer->default_depth,
  );

  $render_array['#attached']['js'][] = array(
    'type' => 'file',
    'data' => drupal_get_path('module', 'graphs_s_tripalphylo') . '/graphs_s_tripalphylo.js',
  );

  return $render_array;
}

/**
 *
 */
function graphs_s_tripalphylo_graphs_get_graph_data($parameters = array()) {

  $graph = array(
    'nodes' => array(),
    'links' => array(),
  );
  
  $node_id = isset($parameters['node_id']) ? $parameters['node_id'] : null;

  // Stop here if no node_id has been specified or if the specified depth is
  // below or equal to 0.
  if (!$node_id
      || (isset($parameters['depth']) && 1 > +$parameters['depth'])) {
    return $graph;
  }

  // Prepare query parts.
  $placeholder_values = array(
    ':phylonode_id' => $node_id,
  );

  // Handle relationships to include.
  $rel_type_statement = '';
  if (isset($parameters['rel_filter']) && count($parameters['rel_filter'])) {
    // We got relationships to take into account.
    $rel_type_statement .= ' AND cr.name IN (:rel_filter) ';
    $placeholder_values[':rel_filter'] = $parameters['rel_filter'];
  }

  // Handle relationships to exclude.
  if (isset($parameters['rel_exclude']) && count($parameters['rel_exclude'])) {
    // We got relationships to exclude.
    $rel_type_statement .= ' AND cr.name NOT IN (:rel_exclude) ';
    $placeholder_values[':rel_exclude'] = $parameters['rel_exclude'];
  }

  $sql_query = '
    SELECT
      p.phylonode_id AS "id",
      p.label AS "name",
      ct.name AS "type",
      p.left_idx AS "left_idx",
      p.right_idx AS "right_idx",
      cr.name AS "relationship",
      p.distance AS "branch_length"
    FROM
      phylonode p
        INNER JOIN cvterm ct ON p.type_id = ct.cvterm_id
        LEFT JOIN phylonode_relationship pr ON
          p.phylonode_id = pr.object_id
          AND pr.subject_id = :phylonode_id
        LEFT JOIN cvterm cr ON cr.cvterm_id = pr.type_id
          ' . $rel_type_statement . '
    WHERE
      p.parent_phylonode_id = :phylonode_id
  ;';

  $results = chado_query($sql_query, $placeholder_values);

  // Save the query to matches.
  $graph_nodes = array();
  foreach ($results as $row) {
    $linked_node_id = $row->id;
    $node = array(
      'id' => $linked_node_id,
      'name' => check_plain($row->name),
      'type' => check_plain($row->type),
      'left_idx' => $row->left_idx,
      'right_idx' => $row->right_idx,
    );
    $graph_nodes[$linked_node_id] = $node;

    $graph['links'][] = array(
      'source' => $node_id,
      'target' => $linked_node_id,
      'relationship' => $row->relationship,
      'branch_length' => $row->branch_length,
    );
  }

  $graph['nodes'] = array_values($graph_nodes);

  // Aggregate sub-levels.
  $skip_node_ids = isset($parameters['skip_node_ids']) ?
    $parameters['skip_node_ids']
    : array();

  $sub_parameters = $parameters;
  if (isset($parameters['depth'])) {
    $sub_parameters['depth'] = $parameters['depth'] - 1;
  }

  if ($graph_nodes) {

    $sub_parameters['skip_node_ids'] = $skip_node_ids + $graph_nodes;

    foreach ($graph_nodes as $sub_node_id => $sub_node) {
      // Skip already processed nodes.
      if (array_key_exists($sub_node_id, $skip_node_ids)) {
        continue;
      }
      $sub_parameters['node_id'] = ''.$sub_node_id;
      // Aggregate.
      $sub_graph = graphs_s_tripalphylo_graphs_get_graph_data($sub_parameters);

      $graph['links'] = array_merge($graph['links'], $sub_graph['links']);
      foreach ($sub_graph['nodes'] as $node) {
        if (!array_key_exists($node['id'], $graph_nodes)) {
          $graph['nodes'][] = $node;
        }
      }
    }
  }

  return $graph;
}
